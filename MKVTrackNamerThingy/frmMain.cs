﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MKVMetaThingyUtils;
using System.IO;

namespace MKVTrackNamerThingy
{
    public partial class frmMain : Form
    {
        #region globals

        TextBox[] videoTextBoxes;
        TextBox[] audioTextBoxes;
        TextBox[] subTextBoxes;
        Label[] videoLabels;
        Label[] audioLabels;
        Label[] subLabels;
        string[,] videoMeta;
        string[,] audioMeta;
        string[,] subMeta;

        TextBox[] videoLangBoxes;
        TextBox[] audioLangBoxes;
        TextBox[] subLangBoxes;

        int filesModified = 0;              // Initialize a counter to count the amount of files successfully modified
        int maxVideoIndex = 0;              // Initialize index of file holding the most video tracks
        int maxAudioIndex = 0;              // Initialize index of file holding the most audio tracks
        int maxSubIndex = 0;                // Initialize index of file holding the most subtitle tracks
        int[] videoCounts;
        int[] audioCounts;
        int[] subCounts;

        #endregion globals

        #region init

        public frmMain()
        {
            InitializeComponent();
            this.Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath);     // Set form icon to one in program's exe
        }

        #endregion init

        #region events

        private void frmMain_Load(object sender, EventArgs e)
        {
            lblAppInfo.Text = Application.ProductName + " ver" + Application.ProductVersion + lblAppInfo.Text;

            if (!MKVToolNix.IsInstalled())          // If MKVToolNix is not installed
            {
                UnlockFormControls(false);          // Lock certain form controls
                MKVToolNix.FoundNotify(false);      // Inform user of the issue
            }
        }
        private async void btnLoad_Click(object sender, EventArgs e) => await LoadFilesAsync();
        private async void btnSave_Click(object sender, EventArgs e) => await SaveAllFilesAsync();
        private async void loadDataFromMKVToolStripMenuItem_Click(object sender, EventArgs e) => await LoadFilesAsync();
        private async void saveDataToMKVToolStripMenuItem_Click(object sender, EventArgs e) => await SaveAllFilesAsync();
        private void exitProgramToolStripMenuItem_Click(object sender, EventArgs e) => Application.Exit();
        private void goToMKVToolNixHomepageToolStripMenuItem_Click(object sender, EventArgs e) => Utils.OpenURL(MKVToolNix.urlHome);
        private void goToMKVToolNixDownloadPageToolStripMenuItem_Click(object sender, EventArgs e) => Utils.OpenURL(MKVToolNix.urlDownload);
        private void recheckToSeeIfMKVToolNixIsInstalledToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool installed = MKVToolNix.IsInstalled();          // Checkif MKVToolNix is installed

            UnlockFormControls(installed);                      // Lock/Unlock form controls based on result
            MKVToolNix.FoundNotify(installed);                  // Display message based on result
        }
        private async void frmMain_DragDrop(object sender, DragEventArgs e) => await LoadFileInfoAsync(((string[])e.Data.GetData(DataFormats.FileDrop)).CleanFileList());
        private void frmMain_DragEnter(object sender, DragEventArgs e) => e.Effect = DragDropEffects.Move;

        #endregion events

        #region methods

        /// <summary>
        /// Method to load files using an OpenFileDialog
        /// </summary>
        //private void LoadFiles()
        //{
        //    DialogResult result = ofdMKV.ShowDialog();      // Show dialog
        //    if (result == DialogResult.OK)                  // If user clicks OK...
        //    {
        //        rtxLog.Clear();                                     // Clear the log
        //        List<string> goodFileNames = new List<string>();    // make new list for fileNames are are actually .mkv files

        //        for (int i = 0; i < ofdMKV.FileNames.Length; i++)   // For each of the selected files...
        //        {
        //            if (ofdMKV.FileNames[i].EndsWith(".mkv"))           // Sanity checking: if the file name ends with .mkv...
        //                goodFileNames.Add(ofdMKV.FileNames[i]);             // Add it to the "good file names" list
        //            else
        //                rtxLog.SendToLog("ERROR!: Selected file is not an .mkv file! Skipping... (" + ofdMKV.FileNames[i] + ")");   // Otherwise print error to log
        //        }

        //        string[] mkvFiles = goodFileNames.ToArray();                 // Convert good file name List to Array for LoadFileInfo()

        //        if (mkvFiles.Length < 1)                            // If there are no filenames in array...
        //            rtxLog.SendToLog("ERROR!: There are no .mkv files selected! Nothing to do.");   // Print error to log regarding this and do nothing else
        //        else
        //            LoadFileInfo(mkvFiles);                             // Else load the array into LoadFileInfo
        //    }
        //}

        private async Task LoadFilesAsync()
        {
            DialogResult result = ofdMKV.ShowDialog();      // Show dialog
            if (result == DialogResult.OK)                  // If user clicks OK...
            {
                rtxLog.Clear();                                     // Clear the log
                List<string> goodFileNames = new List<string>();    // make new list for fileNames are are actually .mkv files

                for (int i = 0; i < ofdMKV.FileNames.Length; i++)   // For each of the selected files...
                {
                    if (ofdMKV.FileNames[i].EndsWith(".mkv"))           // Sanity checking: if the file name ends with .mkv...
                        goodFileNames.Add(ofdMKV.FileNames[i]);             // Add it to the "good file names" list
                    else
                        rtxLog.SendToLog("ERROR!: Selected file is not an .mkv file! Skipping... (" + ofdMKV.FileNames[i] + ")");   // Otherwise print error to log
                }

                string[] mkvFiles = goodFileNames.ToArray();                 // Convert good file name List to Array for LoadFileInfo()

                if (mkvFiles.Length < 1)                            // If there are no filenames in array...
                    rtxLog.SendToLog("ERROR!: There are no .mkv files selected! Nothing to do.");   // Print error to log regarding this and do nothing else
                else
                    await LoadFileInfoAsync(mkvFiles);                             // Else load the array into LoadFileInfo
            }
        }

        /// <summary>
        /// Method to load data from selected files
        /// </summary>
        /// <param name="mkvFiles">Array of selected files' paths</param>
        //private void LoadFileInfo(string[] mkvFiles)
        //{
        //    string exeName = "\"" + MKVToolNix.ExeGetPath(MKVToolNix.mkvmergeExe) + "\"";   // Get path for mkvmerge.exe and wrap in quotes for RunProcess
        //    dynamic json = null;

        //    maxVideoIndex = 0;
        //    maxVideoValue = 0;
        //    maxAudioIndex = 0;                              // Initialize index of file holding the most audio tracks
        //    maxAudioValue = 0;                              // Initialize value of how many audio tracks are in the above described file 
        //    maxSubIndex = 0;                                // Initialize index of file holding the most subtitle tracks
        //    maxSubValue = 0;                                // Initialize value of how many subtitle tracks are in the above described file 

        //    txtFile.Clear();                                    // Clear the file name TextBox

        //    if (mkvFiles.Length > 1)
        //    {
        //        for (int i = 0; i < mkvFiles.Length; i++)           // For each file in the directory...
        //        {
        //            // Set the dynamic json object to...
        //            // ...the output derived from the JSON string...
        //            // ...which is taken from the output from mkvmerge.exe
        //            json = Utils.GetJsonObject(Utils.RunProcess(exeName, "-i -F json \"" + mkvFiles[i] + "\""));

        //            int videoCount = 0;
        //            int audioCount = 0;                             // Initialize audio track counter
        //            int subCount = 0;                               // Initialize subtitle track counter

        //            for (int j = 0; j < json["tracks"].Length; j++)         // For each track in the file...
        //            {
        //                switch ((string)json["tracks"][j]["type"])          // Check the track type
        //                {
        //                    case MKVToolNix.TRACK_VIDEO:
        //                        videoCount++;
        //                        break;

        //                    case MKVToolNix.TRACK_AUDIO:                    // If it is an audio track
        //                        audioCount++;                               // Increment audio track counter
        //                        break;                                      // Break out of switch statement

        //                    case MKVToolNix.TRACK_SUB:                      // If it is a subtitle track
        //                        subCount++;                                 // Increment subtitle track counter
        //                        break;                                      // Break out of switch statement
        //                }
        //            }

        //            if (maxVideoValue < videoCount)
        //            {
        //                maxVideoValue = videoCount;
        //                maxVideoIndex = i;
        //            }

        //            if (maxAudioValue < audioCount)                 // If the amount of audio tracks in this file is greater than current maximum found...
        //            {
        //                maxAudioValue = audioCount;                 // set new maximum to amount found in this file
        //                maxAudioIndex = i;                          // Set the index holding max audio tracks to current file's index
        //            }

        //            if (maxSubValue < subCount)                     // If the amount of subtitle tracks in this file is greater than current maximum found...
        //            {
        //                maxSubValue = subCount;                     // set new maximum to amount found in this file
        //                maxSubIndex = i;                            // Set the index holding max subtitle tracks to current file's index
        //            }

        //            myDebug.WriteLine(Path.GetFileName(mkvFiles[i]) + ":\thas " + audioCount + " audio, and " + subCount + " subs.");

        //            txtFile.AppendText(mkvFiles[i]);                // Add current file's path to file name TextBox
        //            if (i < (mkvFiles.Length - 1))                  // If the file is not the last one...
        //                txtFile.AppendText(Environment.NewLine);    // Also add a new line for nextfile name

        //            lblTitle.Text = "(Multiple files are loaded)";         // Change title label to inform that there are multiple files loadad
        //        }
        //    }
        //    else
        //    {
        //        json = Utils.GetJsonObject(Utils.RunProcess(exeName, "-i -F json \"" + mkvFiles[0] + "\""));

        //        try
        //        {
        //            lblTitle.Text = json["container"]["properties"]["title"];   // Try to display video Title from JSON Object
        //        }
        //        catch (KeyNotFoundException)
        //        {
        //            myDebug.WriteLine("This mkv has no Title set, using blank one... ");
        //            lblTitle.Text = "";                                         // Display blank title if not present
        //        }

        //        txtFile.Text = mkvFiles[0];
        //    }

        //    if (mkvFiles.Length > 1)
        //        json = Utils.GetJsonObject(Utils.RunProcess(exeName, "-i -F json \"" + mkvFiles[maxVideoIndex] + "\""));

        //    videoMeta = InitMetaArray(json, MKVToolNix.TRACK_VIDEO, 2);
        //    InitTrackArrays(json, MKVToolNix.TRACK_VIDEO, out videoLabels, out videoTextBoxes, out videoLangBoxes);
        //    InitPanel(pnlVideo, videoLabels.Length, videoLabels, videoTextBoxes, videoLangBoxes);
        //    GetTracks(json, MKVToolNix.TRACK_VIDEO);

        //    if (mkvFiles.Length > 1 && (maxVideoIndex != maxAudioIndex))
        //    // Get dynamic object from JSON string from mkvmerge.exe for file in folder with max amount of audio tracks
        //        json = Utils.GetJsonObject(Utils.RunProcess(exeName, "-i -F json \"" + mkvFiles[maxAudioIndex] + "\""));

        //    audioMeta = InitMetaArray(json, MKVToolNix.TRACK_AUDIO, 3);                             // Initialize audio meta array
        //    InitTrackArrays(json, MKVToolNix.TRACK_AUDIO, out audioLabels, out audioTextBoxes, out audioLangBoxes);     // Initialize Label and TextBox arrays for the audio tracks
        //    InitPanel(pnlAudio, audioLabels.Length, audioLabels, audioTextBoxes, audioLangBoxes);                   // Initialize the Panel for all the audio track controls
        //    GetTracks(json, MKVToolNix.TRACK_AUDIO);                                                // Get the audio track info

        //    if (mkvFiles.Length > 1 && (maxAudioIndex != maxSubIndex))   // If the "max audio tracks" file and the "max subtitle tracks" file are not the same...
        //                                        // Get dynamic object from JSON string from mkvmerge.exe for file in folder with max amount of subtitle tracks
        //        json = Utils.GetJsonObject(Utils.RunProcess(exeName, "-i -F json \"" + mkvFiles[maxSubIndex] + "\""));

        //    subMeta = InitMetaArray(json, MKVToolNix.TRACK_SUB, 2);                                 // Initialize subtitle meta array
        //    InitTrackArrays(json, MKVToolNix.TRACK_SUB, out subLabels, out subTextBoxes, out subLangBoxes);           // Initialize Label and TextBox arrays for the subtitle tracks
        //    InitPanel(pnlSubtitle, subLabels.Length, subLabels, subTextBoxes, subLangBoxes);                      // Initialize the Panel for all the subtitle track controls
        //    GetTracks(json, MKVToolNix.TRACK_SUB);                                                  // Get the subtitle track info

        //    myDebug.WriteLine(Path.GetFileName(mkvFiles[maxVideoIndex]) + " has thee most Video tracks.");
        //    myDebug.WriteLine(Path.GetFileName(mkvFiles[maxAudioIndex]) + " has the most Audio tracks.");
        //    myDebug.WriteLine(Path.GetFileName(mkvFiles[maxSubIndex]) + " has the most Subtitle tracks.");
        //}

        private void LoadFileTrackCounts(int fileIndex, string path, string exeName)
        {
            // Set the dynamic json object to...
            // ...the output derived from the JSON string...
            // ...which is taken from the output from mkvmerge.exe
            dynamic json = Utils.GetJsonObject(Utils.RunProcess(exeName, "-i -F json \"" + path + "\""));

            int videoCount = 0;
            int audioCount = 0;                             // Initialize audio track counter
            int subCount = 0;                               // Initialize subtitle track counter

            for (int j = 0; j < json["tracks"].Length; j++)         // For each track in the file...
            {
                switch ((string)json["tracks"][j]["type"])          // Check the track type
                {
                    case MKVToolNix.TRACK_VIDEO:
                        videoCount++;
                        break;

                    case MKVToolNix.TRACK_AUDIO:                    // If it is an audio track
                        audioCount++;                               // Increment audio track counter
                        break;                                      // Break out of switch statement

                    case MKVToolNix.TRACK_SUB:                      // If it is a subtitle track
                        subCount++;                                 // Increment subtitle track counter
                        break;                                      // Break out of switch statement
                }
            }

            videoCounts[fileIndex] = videoCount;
            audioCounts[fileIndex] = audioCount;
            subCounts[fileIndex] = subCount;

            myDebug.WriteLine(Path.GetFileName(path) + ":\thas " + audioCount + " audio, and " + subCount + " subs.");
        }

        private async Task LoadFileInfoAsync(string[] mkvFiles)
        {
            UnlockFormControls(false);

            string exeName = "\"" + MKVToolNix.ExeGetPath(MKVToolNix.mkvmergeExe) + "\"";   // Get path for mkvmerge.exe and wrap in quotes for RunProcess
            dynamic json = null;

            // reset max indexes
            maxVideoIndex = 0;
            maxAudioIndex = 0;
            maxSubIndex = 0;

            txtFile.Clear();                                    // Clear the file name TextBox

            if (mkvFiles.Length > 1)
            {
                videoCounts = new int[mkvFiles.Length];
                audioCounts = new int[mkvFiles.Length];
                subCounts = new int[mkvFiles.Length];

                List<Task> fileLoaders = new List<Task>();

                for (int i = 0; i < mkvFiles.Length; i++)           // For each file in the directory...
                {
                    int idx = i;
                    string path = mkvFiles[idx];
                    fileLoaders.Add(Task.Run(() => LoadFileTrackCounts(idx, path, exeName)));
                }

                await Task.WhenAll(fileLoaders);

                for (int i = 0; i < mkvFiles.Length; i++)
                {
                    txtFile.AppendText(mkvFiles[i]);                // Add current file's path to file name TextBox
                    if (i < (mkvFiles.Length - 1))                  // If the file is not the last one...
                        txtFile.AppendText(Environment.NewLine);    // Also add a new line for nextfile name
                }

                maxVideoIndex = Array.IndexOf(videoCounts, videoCounts.Max());
                maxAudioIndex = Array.IndexOf(audioCounts, audioCounts.Max());
                maxSubIndex = Array.IndexOf(subCounts, subCounts.Max());

                lblTitle.Text = "(Multiple files are loaded)";         // Change title label to inform that there are multiple files loadad
            }
            else
            {
                json = Utils.GetJsonObject(Utils.RunProcess(exeName, "-i -F json \"" + mkvFiles[0] + "\""));

                try
                {
                    lblTitle.Text = json["container"]["properties"]["title"];   // Try to display video Title from JSON Object
                }
                catch (KeyNotFoundException)
                {
                    myDebug.WriteLine("This mkv has no Title set, using blank one... ");
                    lblTitle.Text = "";                                         // Display blank title if not present
                }

                txtFile.Text = mkvFiles[0];
            }

            if (mkvFiles.Length > 1)
                json = Utils.GetJsonObject(Utils.RunProcess(exeName, "-i -F json \"" + mkvFiles[maxVideoIndex] + "\""));

            videoMeta = InitMetaArray(json, MKVToolNix.TRACK_VIDEO, 2);
            InitTrackArrays(json, MKVToolNix.TRACK_VIDEO, out videoLabels, out videoTextBoxes, out videoLangBoxes);
            InitPanel(pnlVideo, videoLabels.Length, videoLabels, videoTextBoxes, videoLangBoxes);
            GetTracks(json, MKVToolNix.TRACK_VIDEO);

            if (mkvFiles.Length > 1 && (maxVideoIndex != maxAudioIndex))
                // Get dynamic object from JSON string from mkvmerge.exe for file in folder with max amount of audio tracks
                json = Utils.GetJsonObject(Utils.RunProcess(exeName, "-i -F json \"" + mkvFiles[maxAudioIndex] + "\""));

            audioMeta = InitMetaArray(json, MKVToolNix.TRACK_AUDIO, 3);                             // Initialize audio meta array
            InitTrackArrays(json, MKVToolNix.TRACK_AUDIO, out audioLabels, out audioTextBoxes, out audioLangBoxes);     // Initialize Label and TextBox arrays for the audio tracks
            InitPanel(pnlAudio, audioLabels.Length, audioLabels, audioTextBoxes, audioLangBoxes);                   // Initialize the Panel for all the audio track controls
            GetTracks(json, MKVToolNix.TRACK_AUDIO);                                                // Get the audio track info

            if (mkvFiles.Length > 1 && (maxAudioIndex != maxSubIndex))   // If the "max audio tracks" file and the "max subtitle tracks" file are not the same...
                                                                         // Get dynamic object from JSON string from mkvmerge.exe for file in folder with max amount of subtitle tracks
                json = Utils.GetJsonObject(Utils.RunProcess(exeName, "-i -F json \"" + mkvFiles[maxSubIndex] + "\""));

            subMeta = InitMetaArray(json, MKVToolNix.TRACK_SUB, 2);                                 // Initialize subtitle meta array
            InitTrackArrays(json, MKVToolNix.TRACK_SUB, out subLabels, out subTextBoxes, out subLangBoxes);           // Initialize Label and TextBox arrays for the subtitle tracks
            InitPanel(pnlSubtitle, subLabels.Length, subLabels, subTextBoxes, subLangBoxes);                      // Initialize the Panel for all the subtitle track controls
            GetTracks(json, MKVToolNix.TRACK_SUB);                                                  // Get the subtitle track info

            myDebug.WriteLine(Path.GetFileName(mkvFiles[maxVideoIndex]) + " has thee most Video tracks.");
            myDebug.WriteLine(Path.GetFileName(mkvFiles[maxAudioIndex]) + " has the most Audio tracks.");
            myDebug.WriteLine(Path.GetFileName(mkvFiles[maxSubIndex]) + " has the most Subtitle tracks.");

            UnlockFormControls(true);
        }

        /// <summary>
        /// Method to save the track names from form to the MKV file(s)
        /// </summary>
        //private void SaveTrackNames()
        //{
        //    UnlockFormControls(false);

        //    if (txtFile.Text != "")             // If the file name TextBox actually has something in it...
        //    {
        //        rtxLog.Text = "";                   // Clear the log

        //        int filesModified = 0;              // Initialize a counter to count the amount of files successfully modified
        //        string retStr;                      // Declare string for RunProcess output
        //        string[] mkvFiles = txtFile.Text.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);  // Build array of filenames from file name TextBox
        //        string exeName = "\"" + MKVToolNix.ExeGetPath(MKVToolNix.mkvpropeditExe) + "\"";    // Get path for mkvpropedit.exe and wrap in quotes for use as RunProcess arg
                
        //        StringBuilder args = new StringBuilder();           // Make new StringBuilder for arguments
        //        if (chkVideoActivated.Checked)
        //            args.Append(BuildArgs(videoTextBoxes, videoLangBoxes, false));
        //        if (chkAudioActivated.Checked)
        //            args.Append(BuildArgs(audioTextBoxes, audioLangBoxes, false));      // Add arguments for audio tracks
        //        if (chkSubActivated.Checked)
        //            args.Append(BuildArgs(subTextBoxes, subLangBoxes, false));        // Add arguments for subtitle tracks

        //        if (mkvFiles.Length > 1)                            // If there is more than one file to modify...
        //        {
        //            for (int i = 0; i < mkvFiles.Length; i++)           // For each file...
        //            {
        //                myDebug.WriteLine(mkvFiles[i]);
        //                string fileName = "\"" + mkvFiles[i] + "\"";    // Wrap file name in quotes for use in args

        //                rtxLog.SendToLog("File No." + (i + 1) + " of " + mkvFiles.Length + ": " + mkvFiles[i]); // Send to log file no. and name
        //                rtxLog.SendToLog(exeName + " " + fileName + args.ToString());   // Send to log the complete command sent to mkvpropedit
        //                retStr = Utils.RunProcess(exeName, fileName + args.ToString()); // Run mkvpropedit with final generated args
        //                rtxLog.SendToLog(retStr);                       // send returned result from mkvpropedit to log

        //                int goodVideoCount = videoTextBoxes.Length;
        //                int goodAudioCount = audioTextBoxes.Length;     // Initialize count of "good" audio, count of audio tracks found in file (needed for batch mode, may not have all tracks in selected file) 
        //                int goodSubCount = subTextBoxes.Length;         // Initialize count of "good" subtitle, count of subtitle tracks found in file
        //                StringBuilder argsAlt;                          // Declare another StringBuilder for an alternate final arg (used for batch mode, in case some tracks are not found)

        //                filesModified++;                                // Increment filesModified

        //                while (retStr != MKVToolNix.mkvpropedit_OK)                                                               // While mkvpropedit does not return the "everything completed OK" result
        //                {
        //                    if (retStr.Contains(MKVToolNix.mkvpropedit_ERROR_CannotOpenFile))  //If file couldnot be accessed...
        //                    {
        //                        rtxLog.SendToLog("ERROR: could not seem to access this file, skipping it...");  // Inform user if the issue
        //                        filesModified--;    // decrement filesModified
        //                        break;              // Break out of while loop, skipping file.
        //                    }

        //                    if (retStr.Contains(MKVToolNix.mkvPropedit_ERROR_MissingVideoTrack))
        //                        goodVideoCount--;
        //                    else if (retStr.Contains(MKVToolNix.mkvPropedit_ERROR_MissingAudioTrack))          // If returned error was an audio track not found...
        //                        goodAudioCount--;                                       // Means the last audio track was not found, so decrement good audio count
        //                    else if (retStr.Contains(MKVToolNix.mkvPropedit_ERROR_MissingSubTrack))     // If returned error was a subtitle track not found...
        //                        goodSubCount--;                                         // Means the last subtitle track was not found, so decrement good subtitle count

        //                    argsAlt = new StringBuilder();                                          // Instantiate new StringBuilder for alternate args
        //                    argsAlt.Append(BuildArgs(videoTextBoxes, videoLangBoxes, false, goodVideoCount));
        //                    argsAlt.Append(BuildArgs(audioTextBoxes, audioLangBoxes, false, goodAudioCount));       // Build new audio tracks args based on updated good audio count
        //                    argsAlt.Append(BuildArgs(subTextBoxes, subLangBoxes, false, goodSubCount));           // Build new subtitle track args based on updated good subtitle count

        //                    rtxLog.SendToLog(exeName + " " + fileName + argsAlt.ToString());        // Send new complete command to log
        //                    retStr = Utils.RunProcess(exeName, fileName + argsAlt.ToString());      // Try running mkvpropedit with new args
        //                    rtxLog.SendToLog(retStr);                                               // send result from mkvpropedit to log
        //                }
        //            }
        //            // Finally show message to user when batch operation is done including count of files successfully modified
        //            MessageBox.Show(filesModified + " of " + mkvFiles.Length + " files successfully modified!" + Environment.NewLine +
        //            Environment.NewLine +
        //            "Check the log for more details.", "Batch save complete!", MessageBoxButtons.OK, MessageBoxIcon.Information);

        //            await LoadFileInfoAsync(mkvFiles);             // Reload the files after modification
        //        }
        //        else
        //        {
        //            string fileName = mkvFiles[0];                                          // If there is only one file, just get filename from array
        //            string wrappedFileName = "\"" + fileName + "\"";                        // Wrap filename in quotes for RunProcess arg
        //            rtxLog.SendToLog("File: " + wrappedFileName);                           // Send to log file name
        //            rtxLog.SendToLog(exeName + " " + wrappedFileName + args.ToString());    // Send to log the complete command sent to mkvpropedit
        //            retStr = Utils.RunProcess(exeName, wrappedFileName + args.ToString());  // Run mkvpropedit with final generated args
        //            rtxLog.SendToLog(retStr);                                               // send returned result from mkvpropedit to log
        //            await LoadFileInfoAsync(mkvFiles);                                                 // Reload file after modification
        //        }
        //    }

        //    UnlockFormControls(true);
        //}

        private string SaveFileAsync(string path, string args, string exeName, int idx, int count)
        {
            StringBuilder result = new StringBuilder();
            string retStr = "";
            string fileName = "\"" + path + "\"";    // Wrap file name in quotes for use in args

            result.AppendLine($"File No.{idx + 1} of {count}: {path}"); // Send to log file no. and name
            result.AppendLine($"{exeName} {fileName}{args}");   // Send to log the complete command sent to mkvpropedit
            retStr = Utils.RunProcess(exeName, fileName + args.ToString()); // Run mkvpropedit with final generated args
            result.AppendLine(retStr);                       // send returned result from mkvpropedit to log

            int goodVideoCount = videoTextBoxes.Length;
            int goodAudioCount = audioTextBoxes.Length;     // Initialize count of "good" audio, count of audio tracks found in file (needed for batch mode, may not have all tracks in selected file) 
            int goodSubCount = subTextBoxes.Length;         // Initialize count of "good" subtitle, count of subtitle tracks found in file
            StringBuilder argsAlt;                          // Declare another StringBuilder for an alternate final arg (used for batch mode, in case some tracks are not found)

            filesModified++;                                // Increment filesModified

            while (retStr != MKVToolNix.mkvpropedit_OK)                                                               // While mkvpropedit does not return the "everything completed OK" result
            {
                if (retStr.Contains(MKVToolNix.mkvpropedit_ERROR_CannotOpenFile))  //If file couldnot be accessed...
                {
                    result.AppendLine("ERROR: could not seem to access this file, skipping it...");  // Inform user if the issue
                    filesModified--;    // decrement filesModified
                    break;              // Break out of while loop, skipping file.
                }

                if (retStr.Contains(MKVToolNix.mkvPropedit_ERROR_MissingVideoTrack))
                    goodVideoCount--;
                else if (retStr.Contains(MKVToolNix.mkvPropedit_ERROR_MissingAudioTrack))          // If returned error was an audio track not found...
                    goodAudioCount--;                                       // Means the last audio track was not found, so decrement good audio count
                else if (retStr.Contains(MKVToolNix.mkvPropedit_ERROR_MissingSubTrack))     // If returned error was a subtitle track not found...
                    goodSubCount--;                                         // Means the last subtitle track was not found, so decrement good subtitle count

                argsAlt = new StringBuilder();                                          // Instantiate new StringBuilder for alternate args
                argsAlt.Append(BuildArgs(videoTextBoxes, videoLangBoxes, false, goodVideoCount));
                argsAlt.Append(BuildArgs(audioTextBoxes, audioLangBoxes, false, goodAudioCount));       // Build new audio tracks args based on updated good audio count
                argsAlt.Append(BuildArgs(subTextBoxes, subLangBoxes, false, goodSubCount));           // Build new subtitle track args based on updated good subtitle count

                result.AppendLine($"{exeName} {fileName}{argsAlt.ToString()}");        // Send new complete command to log
                retStr = Utils.RunProcess(exeName, fileName + argsAlt.ToString());      // Try running mkvpropedit with new args
                result.AppendLine(retStr);                                               // send result from mkvpropedit to log
            }
            //result += Environment.NewLine;

            return result.ToString();
        }

        private async Task SaveAllFilesAsync()
        {
            UnlockFormControls(false);

            if (txtFile.Text != "")             // If the file name TextBox actually has something in it...
            {
                rtxLog.Text = "";                   // Clear the log

                string retStr;                      // Declare string for RunProcess output
                string[] mkvFiles = txtFile.Text.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);  // Build array of filenames from file name TextBox
                string exeName = "\"" + MKVToolNix.ExeGetPath(MKVToolNix.mkvpropeditExe) + "\"";    // Get path for mkvpropedit.exe and wrap in quotes for use as RunProcess arg

                StringBuilder args = new StringBuilder();           // Make new StringBuilder for arguments
                if (chkVideoActivated.Checked)
                    args.Append(BuildArgs(videoTextBoxes, videoLangBoxes, false));
                if (chkAudioActivated.Checked)
                    args.Append(BuildArgs(audioTextBoxes, audioLangBoxes, false));      // Add arguments for audio tracks
                if (chkSubActivated.Checked)
                    args.Append(BuildArgs(subTextBoxes, subLangBoxes, false));        // Add arguments for subtitle tracks

                if (args.Length > 0)
                {
                    if (mkvFiles.Length > 1)                            // If there is more than one file to modify...
                    {
                        filesModified = 0;
                        List<Task<string>> filesSaving = new List<Task<string>>();

                        for (int i = 0; i < mkvFiles.Length; i++)           // For each file...
                        {
                            int idx = i;
                            int count = mkvFiles.Length;
                            string path = mkvFiles[i];
                            string argsStr = args.ToString();
                            myDebug.WriteLine(path);
                            filesSaving.Add(Task.Run(() => SaveFileAsync(path, argsStr, exeName, idx, count)));
                        }
                        var results = await Task.WhenAll(filesSaving);

                        foreach (string s in results)
                            rtxLog.AppendText(s);

                        // Finally show message to user when batch operation is done including count of files successfully modified
                        MessageBox.Show(filesModified + " of " + mkvFiles.Length + " files successfully modified!" + Environment.NewLine +
                        Environment.NewLine +
                        "Check the log for more details.", "Batch save complete!", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        await LoadFileInfoAsync(mkvFiles);             // Reload the files after modification
                    }
                    else
                    {
                        string fileName = mkvFiles[0];                                          // If there is only one file, just get filename from array
                        string wrappedFileName = "\"" + fileName + "\"";                        // Wrap filename in quotes for RunProcess arg
                        rtxLog.SendToLog("File: " + wrappedFileName);                           // Send to log file name
                        rtxLog.SendToLog(exeName + " " + wrappedFileName + args.ToString());    // Send to log the complete command sent to mkvpropedit
                        retStr = Utils.RunProcess(exeName, wrappedFileName + args.ToString());  // Run mkvpropedit with final generated args
                        rtxLog.SendToLog(retStr);                                               // send returned result from mkvpropedit to log
                        await LoadFileInfoAsync(mkvFiles);                                                 // Reload file after modification
                    }
                }
                else
                {
                    string fail = "None of the Track Editor Panels have been activated! Nothing to do.";
                    MessageBox.Show(fail, "Batch save failed!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            UnlockFormControls(true);
        }

        /// <summary>
        /// Method to get track information from dynamic object and add it to form controls
        /// </summary>
        /// <param name="json">the dynamic json object obtained from GetJsonObject()</param>
        private void GetTracks(dynamic json, string trackType)
        {
            int count = 0;                                                      // Initialize count
            int trackCount = json["tracks"].Length;                             // Get amount of tracks

            for (int i = 0; i < trackCount; i++)                                // For each track...
            {
                if ((string)json["tracks"][i]["type"] == trackType)                 // If the current track is the specified type...
                {
                    string trackName;                                               // Declare trackName var
                    string trackLang = json["tracks"][i]["properties"]["language"];   // Get track language from dynamic object
                    string trackCodec = (string)json["tracks"][i]["codec"];         //Get track Codec from dynamic object
                    if (trackCodec == "AC-3/E-AC-3")
                        trackCodec = "DD";

                    try
                    {
                        trackName = json["tracks"][i]["properties"]["track_name"];  // Try to get track name from dynamic object
                    }
                    catch (KeyNotFoundException)
                    {
                        myDebug.WriteLine("Track No." + i + " has no name... using blank name instead.");
                        trackName = "";                                             // If that fails, means that it doesn't exist, so use blank name instead
                    }

                    switch (trackType)
                    {
                        case MKVToolNix.TRACK_VIDEO:
                            videoMeta[count, 0] = trackLang;
                            videoMeta[count, 1] = trackCodec;
                            myDebug.WriteLine(videoMeta[count, 0] + " - " + videoMeta[count, 1]);
                            videoLabels[count].Text = videoMeta[count, 0] + " - " + videoMeta[count, 1];  // Set label text for track to string made from the track's Metadata
                            videoTextBoxes[count].Text = trackName;       // Put the track's name in the track's TextBox
                            videoLangBoxes[count].Text = trackLang;
                            break;

                        case MKVToolNix.TRACK_AUDIO:                    // If track is an audio track...
                            int channels = (int)json["tracks"][i]["properties"]["audio_channels"];  // Get the amount if audio channels from dynamic object
                            string audioChan = "";                      // Init string representing audio channel type

                            switch (channels)                           // assign description to audioChan depending on the amount of channels in audio
                            {
                                case 2:
                                    audioChan = "Stereo";
                                    break;

                                case 6:
                                    audioChan = "Surround 5.1";
                                    break;

                                case 8:
                                    audioChan = "Surround 7.1";
                                    break;

                                default:
                                    audioChan = channels + "-Channel";
                                    break;
                            }

                            audioMeta[count, 0] = trackLang;            // Add track language code to audioMeta array
                            audioMeta[count, 1] = trackCodec;           // Add track codec to audioMeta array
                            audioMeta[count, 2] = audioChan;            // Add channels description to audioMeta array
                            myDebug.WriteLine(audioMeta[count, 0] + " - " + audioMeta[count, 1] + " - " + audioMeta[count, 2]);
                            audioLabels[count].Text = audioMeta[count, 0] + " - " + audioMeta[count, 1] + " - " + audioMeta[count, 2];  // Set label text for track to string made from the track's Metadata
                            audioTextBoxes[count].Text = trackName;     // Put the track's name in the track's TextBox
                            audioLangBoxes[count].Text = trackLang;
                            count++;                                    // Increment count
                            break;

                        case MKVToolNix.TRACK_SUB:                      // If track is a subtitle track...
                            subMeta[count, 0] = trackLang;              // Add track language code to subMeta array
                            subMeta[count, 1] = trackCodec;             // Add track codec to subMeta array
                            myDebug.WriteLine(subMeta[count, 0] + " - " + subMeta[count, 1]);
                            subLabels[count].Text = subMeta[count, 0] + " - " + subMeta[count, 1];  // Set label text for track to string made from the track's Metadata
                            subTextBoxes[count].Text = trackName;       // Put the track's name in the track's TextBox
                            subLangBoxes[count].Text = trackLang;
                            count++;                                    // Increment count
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Method to initialize a panel with its various controls
        /// </summary>
        /// <param name="pnl">The Panel to initialize</param>
        /// <param name="trackCount">The amount of tracks going into the Panel</param>
        /// <param name="labelArray">The array for the Labels going in this Panel</param>
        /// <param name="txtNameArray">The array for the TextBoxes going in this Panel</param>
        private void InitPanel(Panel pnl, int trackCount, Label[] labelArray, TextBox[] txtNameArray, TextBox[] txtLangArray)
        {
            const int left = 0;                                       // Initialize vars to define some distances
            const int labelTop = 0;
            const int textBoxTop = labelTop + 25;
            const int controlGap = textBoxTop + 40;
            const int trackNoWidth = 75;

            if (pnl.Controls.Count > 0)                         // If there are already controls in this Panel
            {
                pnl.Controls.Clear();                           // Clear controls from Panel
                for (int i = pnl.Controls.Count - 1; i >= 0; i--)   // For each of the Panel's Controls... (Need reverse for loop for Control.Dispose())
                    pnl.Controls[i].Dispose();                  // Dispose of it to remove it from memory
            }

            for (int i = 0; i < trackCount; i++)                        // For each of the tracks...
            {
                pnl.Controls.Add(new Label                              // Make new Label for Track No. and add to Panel
                {
                    Left = left,                                        // Set X coordinate location in panel for this Label
                    Top = labelTop + (controlGap * i),                  // Set Y coordinate location in panel for this Label
                    Text = "Track No." + (i + 1) + ":",                 // Add respective text to Label
                    Width = trackNoWidth                                // Set width for Label
                });                               

                labelArray[i] = new Label                               // Make new Label for current index in LabelArray
                {
                    Left = left + trackNoWidth + 10,                    // Set X coordinate location in panel for this Label
                    Top = labelTop + (controlGap * i),                  // Set Y coordinate location in panel for this Label
                    Width = 220                                         // Set width for Label
                };
                pnl.Controls.Add(labelArray[i]);                        // Add the control to the Panel                     

                txtNameArray[i] = new TextBox                           // Make new TextBox for current index in TextBoxArray
                {
                    Left = left,                                        // Set X coordinate location in panel for this TextBox
                    Top = textBoxTop + (controlGap * i),                // Set Y coordinate location in panel for this TextBox
                    Width = 210                                         // Set width for TextBox
                };
                pnl.Controls.Add(txtNameArray[i]);                      // Add the control to the Panel                    

                pnl.Controls.Add(new Label
                {
                    Text = "Lang Code:",
                    Left = left + 215,
                    Top = textBoxTop + (controlGap * i) + 3,
                    Width = 65
                });

                txtLangArray[i] = new TextBox
                {
                    Left = left + 280,
                    Top = textBoxTop + (controlGap * i),
                    Width = 40
                };
                pnl.Controls.Add(txtLangArray[i]);
            }
        }

        /// <summary>
        /// Method to initialize a 2D arrayfor holding metadata for each track
        /// </summary>
        /// <param name="json">dynamic object holding data about the mkv file</param>
        /// <param name="type">"audio" fo audio, "subtitles" for subtitles</param>
        /// <returns>A 2D array ready to be populated with metadata for the tracks</returns>
        private string[,] InitMetaArray(dynamic json, string trackType, int arrayFields)
        {
            int count = 0;                                              // Initialize a count
            string[,] meta;                                             // Declare the 2D array

            for (int i = 0; i < json["tracks"].Length; i++)             // For each track...
                if ((string)json["tracks"][i]["type"] == trackType)         // If the current track is the specified tyoe...
                    count++;                                                // Increment the count

            meta = new string[count, arrayFields];                      // Instantiate new 2D array with supplied sizes
            return meta;                                                // return the created array
        }

        /// <summary>
        /// Method to initializethe TextBox and Label arrays for each track type
        /// </summary>
        /// <param name="json">dynamic object holding data about the mkv file</param>
        /// <param name="trackType">A string identifying what track type to use</param>
        /// <param name="labels">The Label Array to init</param>
        /// <param name="textBoxes">The TextBox Array to init</param>
        private void InitTrackArrays(dynamic json, string trackType, out Label[] labels, out TextBox[] textBoxes, out TextBox[] langBoxes)
        {
            int count = 0;                                              // Initialize a count
            for (int i = 0; i < json["tracks"].Length; i++)                 // For each of the tracks...
                if ((string)json["tracks"][i]["type"] == trackType)         // If the current track is the specified tyoe...
                    count++;                                                // Increment the count

            labels = new Label[count];                                  // Create new Label array with count as its size
            textBoxes = new TextBox[count];                             // Create new TextBox array with count as its size
            langBoxes = new TextBox[count];
        }

        /// <summary>
        /// Method to build the arguments to tell mkvpropedit to edit which tracks, and with what values
        /// </summary>
        /// <param name="textBoxArray">The array of TextBoxes to hold names for the tracks (either audio or subtitle)</param>
        /// <param name="addSetting">Boolean to specify whether the argument edits the existing setting in the track in file, or adds it anew. Used for when the setting does not exist in the track in file.</param>
        /// <param name="trackCount">Optional. Used to specify how many tracks in the Array are being processed. Will use the TextBox Array's Item Count as default.</param>
        /// <returns>String of arguments for editing the tracks in the ListBox</returns>
        private string BuildArgs(TextBox[] textBoxArray, TextBox[] langBoxArray, bool addSetting, int trackCount = 0)
        {
            if (trackCount == 0)                                    // If trackCount is defaul sentinel value of zero
                trackCount = textBoxArray.Length;                   // Set trackCount to amount of TextBoxes

            string arg = "";                                        // initialize the string vars, the arg, its track type, and its setting type
            string tType = "";                                      
            string sType = "";

            if (textBoxArray.Equals(videoTextBoxes))
                tType = "v";
            else if (textBoxArray.Equals(audioTextBoxes))                // If the TextBox array is the audioTextBoxes...
                tType = "a";                                        // set arg track type to audio
            else if (textBoxArray.Equals(subTextBoxes))             // Else If the TextBox array is the audioTextBoxes...
                tType = "s";                                        // set arg track type to subtitle

            for (int i = 0; i < trackCount; i++)                        // For each of the tracks (up to the specified count)...
            {
                if (addSetting)                                         // If addSetting is true...
                    sType = "a";                                        // Set argument's setting type to "add"
                else
                    sType = "s";                                        // Otherwise set argument's setting type to "set"

                StringBuilder name = new StringBuilder();               // Instantiate new StringBuilder to construct track name
                switch(tType)
                {
                    case "v":
                        if (chkVideoPrepLang.Checked)
                            name.Append(videoMeta[i, 0] + " ");

                        if (chkVideoPrepCodec.Checked)
                            name.Append(videoMeta[i, 1] + " ");
                        break;

                    case "a":                                           // If it is an audio track...
                        if (chkAudioPrepLang.Checked)                   // If Audio Prepend Lang code is checked...
                            name.Append(audioMeta[i, 0] + " ");         // Get Lang code from audio meta array and add to name

                        if (chkAudioPrepCodec.Checked)                  // If Audio Prepend Codec is checked...
                            name.Append(audioMeta[i, 1] + " ");         // Get Codec from audio meta array and add to name

                        if (chkAudioPrepChan.Checked)                   // Get Audio Prepend Channels is checked...
                            name.Append(audioMeta[i, 2] + " ");         // Get Channels from audio meta array and add to name
                        break;

                    case "s":                                           // If it is a subtitle track...
                        if (chkSubPrepLang.Checked)                     // If Subtitle Prepend Lang code is checked...
                            name.Append(subMeta[i, 0] + " ");           // Get Lang code from subtitle meta array and add to name

                        if (chkSubPrepCodec.Checked)                    // If Subtitle Prepend Codec is checked...
                            name.Append(subMeta[i, 1] + " ");           // Get Codec from subtitle meta array and add to name
                        break;
                }

                if (textBoxArray[i].Text == "" && name.Length > 0)      // If Name TextBox for this track is empty but prefill fields are checked...
                    name.Remove(name.Length-1, 1);                      // remove the last trailing space from name
                else
                    name.Append(textBoxArray[i].Text);                  // Otherwise add contents of TextBox to name

                myDebug.WriteLine(name.ToString());

                // append constructed argument for this track to arg
                arg += " -e track:" + tType + (i + 1) + " -" + sType + " \"name=" + name.ToString() + "\"";

                if (langBoxArray[i].Text.Length == 3)
                    arg += " --set language=" + langBoxArray[i].Text.ToLower();
            }
            return arg;                                             // return the completed arg
        }

        /// <summary>
        /// Method to unlock certain form controls based onbool value passed to it
        /// </summary>
        /// <param name="unlocked">Boolean representing whether to lock or unlock certain controls</param>
        private void UnlockFormControls(bool unlocked)
        {
            loadDataFromMKVToolStripMenuItem.Enabled = unlocked;                    // Lock/Unlock these controls based on passed unlocked value
            saveDataToMKVToolStripMenuItem.Enabled = unlocked;
            btnLoad.Enabled = unlocked;
            btnSave.Enabled = unlocked;
        }

        #endregion methods
    }
}
