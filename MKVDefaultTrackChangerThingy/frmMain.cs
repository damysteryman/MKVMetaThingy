﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Collections;

using MKVMetaThingyUtils;

namespace MKVDefaultTrackChangerThingy
{
    public partial class frmMain : Form
    {
        #region globals

        int filesModified = 0;              // Initialize a counter to count the amount of files successfully modified
        int maxVideoIndex = 0;              // Initialize index of file holding the most video tracks
        int maxAudioIndex = 0;              // Initialize index of file holding the most audio tracks
        int maxSubIndex = 0;                // Initialize index of file holding the most subtitle tracks
        int[] videoCounts;
        int[] audioCounts;
        int[] subCounts;

        #endregion globals

        #region init

        /// <summary>
        /// Main constructor for form
        /// </summary>
        public frmMain()
        {
            InitializeComponent();
            this.Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath);     // Set form icon to one in program's exe
        }

        #endregion init

        #region events

        private void frmMain_Load(object sender, EventArgs e)
        {
            // Add application information to lblAppInfo label
            lblAppInfo.Text = Application.ProductName + " ver" + Application.ProductVersion + lblAppInfo.Text;

            if(!MKVToolNix.IsInstalled())           // Check for MKVTOolNix, and if MKVToolNix is not found in computer...
            {
                UnlockFormControls(false);          // Lock some form controls
                MKVToolNix.FoundNotify(false);      // Notify the user of the result of the check
            }
        }
        private async void btnLoad_Click(object sender, EventArgs e) => await LoadFileAsync();                             // Load track info from file(s)
        private async void btnSave_Click(object sender, EventArgs e) => await SaveAllFilesAsync();                      // Save the selecetd settings to file(s)
        private async void loadToolStripMenuItem_Click(object sender, EventArgs e) => await LoadFileAsync();                             // Load track info from file(s)
        private async void saveToolStripMenuItem_Click(object sender, EventArgs e) => await SaveAllFilesAsync();                      // Save the selecetd settings to file(s)
        private void exitProgramToolStripMenuItem_Click(object sender, EventArgs e) => Application.Exit();                     // Exit Program
        private void goToMKVToolNixsHomepageToolStripMenuItem_Click(object sender, EventArgs e) => Utils.OpenURL(MKVToolNix.urlHome);      // Open the URL to the MKVToolNix Homepage in user's default browser
        private void goToMKVToolNixdownloadPageToolStripMenuItem_Click(object sender, EventArgs e) => Utils.OpenURL(MKVToolNix.urlDownload);  // Open the URL to the MKVToolNix Download Page in user's default browser
        private async void frmMain_DragDrop(object sender, DragEventArgs e) => await LoadFileInfoAsync(((string[])e.Data.GetData(DataFormats.FileDrop)).CleanFileList());
        private void frmMain_DragEnter(object sender, DragEventArgs e) => e.Effect = DragDropEffects.Move;
        /// <summary>
        /// Event that rechecks if MKVToolNix is installed, notifies the user of the result, and locks/unlocks some form controls depending on the result.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void recheckToSeeIfMKVToolNixIsInstalledOnThisMachineToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool installed = MKVToolNix.IsInstalled();      // Check if MKVToolNix is installed

            UnlockFormControls(installed);                  // Lock or Unlock certain form controls depending on result of above
            MKVToolNix.FoundNotify(installed);              // Display message to user depending on result of above
        }
        private void llbMKVToolNix_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) => Utils.OpenURL(MKVToolNix.urlHome);      // Open the URL to the MKVToolNix Homepage in user's default browser
        private void dgvAudio_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            dgv.CurrentCell = dgv.Rows[e.RowIndex].Cells[0];
        }
        private void frmMain_Resize(object sender, EventArgs e) => txtFile.Width = Width - 60;

        #endregion events

        #region methods

        private IList GetTracks(dynamic json, string type = "")
        {
            IList tracks = new List<Track>();

            switch (type)
            {
                case MKVToolNix.TRACK_VIDEO:
                    tracks = new List<VideoTrack>();
                    break;

                case MKVToolNix.TRACK_AUDIO:
                    tracks = new List<AudioTrack>();
                    break;

                case MKVToolNix.TRACK_SUB:
                    tracks = new List<SubtitleTrack>();
                    break;

                default: // if track type is unsupported/not specified, just get and retun all of them
                    for (int i = 0; i < json["tracks"].Length; i++)
                        tracks.Add(TrackFactory.MakeTrack(json["tracks"][i]));
                    return tracks;
            }   

            for (int i = 0; i < json["tracks"].Length; i++)
            {
                if (json["tracks"][i]["type"] == type)
                    tracks.Add(TrackFactory.MakeTrack(json["tracks"][i]));
            }

            return tracks;
        }

        /// <summary>
        /// Method to load files using an OpenFileDialog
        /// </summary>
        private async Task LoadFileAsync()
        {
            DialogResult result = ofdMKV.ShowDialog();      // Show dialog
            if (result == DialogResult.OK)                  // If user clicks OK...
            {
                rtxLog.Clear();                                     // Clear the log
                List<string> goodFileNames = new List<string>();    // make new list for fileNames are are actually .mkv files

                for (int i = 0; i < ofdMKV.FileNames.Length; i++)   // For each of the selected files...
                {
                    if (ofdMKV.FileNames[i].EndsWith(".mkv"))           // Sanity checking: if the file name ends with .mkv...
                        goodFileNames.Add(ofdMKV.FileNames[i]);             // Add it to the "good file names" list
                    else
                        rtxLog.SendToLog("ERROR!: Selected file is not an .mkv file! Skipping... (" + ofdMKV.FileNames[i] + ")");   // Otherwise print error to log
                }

                string[] mkvFiles = goodFileNames.ToArray();                 // Convert good file name List to Array for LoadFileInfo()

                if (mkvFiles.Length < 1)                            // If there are no filenames in array...
                    rtxLog.SendToLog("ERROR!: There are no .mkv files selected! Nothing to do.");   // Print error to log regarding this and do nothing else
                else
                    await LoadFileInfoAsync(mkvFiles);                             // Else load the array into LoadFileInfo
            }
        }

        private void LoadFileTrackCounts(int fileIndex, string path, string exeName)
        {
            // Set the dynamic json object to...
            // ...the output derived from the JSON string...
            // ...which is taken from the output from mkvmerge.exe
            dynamic json = Utils.GetJsonObject(Utils.RunProcess(exeName, "-i -F json \"" + path + "\""));

            int videoCount = 0;
            int audioCount = 0;                             // Initialize audio track counter
            int subCount = 0;                               // Initialize subtitle track counter

            for (int j = 0; j < json["tracks"].Length; j++)         // For each track in the file...
            {
                switch ((string)json["tracks"][j]["type"])          // Check the track type
                {
                    case MKVToolNix.TRACK_VIDEO:
                        videoCount++;
                        break;

                    case MKVToolNix.TRACK_AUDIO:                    // If it is an audio track
                        audioCount++;                               // Increment audio track counter
                        break;                                      // Break out of switch statement

                    case MKVToolNix.TRACK_SUB:                      // If it is a subtitle track
                        subCount++;                                 // Increment subtitle track counter
                        break;                                      // Break out of switch statement
                }
            }

            videoCounts[fileIndex] = videoCount;
            audioCounts[fileIndex] = audioCount;
            subCounts[fileIndex] = subCount;

            myDebug.WriteLine(Path.GetFileName(path) + ":\thas " + audioCount + " audio, and " + subCount + " subs.");
        }

        /// <summary>
        /// Method to load data from selected files
        /// </summary>
        /// <param name="mkvFiles">Array of selected files' paths</param>
        private async Task LoadFileInfoAsync(string[] mkvFiles)
        {
            UnlockFormControls(false);

            string exeName = "\"" + MKVToolNix.ExeGetPath(MKVToolNix.mkvmergeExe) + "\"";   // Get path for mkvmerge.exe and wrap in quotes for RunProcess
            dynamic json = null;

            txtFile.Clear();                                    // Clear the file name TextBox
            maxVideoIndex = 0;
            maxAudioIndex = 0;                              // Initialize index of file holding the most audio tracks 
            maxSubIndex = 0;                                // Initialize index of file holding the most subtitle tracks

            videoCounts = new int[mkvFiles.Length];
            audioCounts = new int[mkvFiles.Length];
            subCounts = new int[mkvFiles.Length];

            List<Task> fileLoaders = new List<Task>();

            for (int i = 0; i < mkvFiles.Length; i++)           // For each file in the directory...
            {
                int idx = i;
                string path = mkvFiles[idx];
                fileLoaders.Add(Task.Run(() => LoadFileTrackCounts(idx, path, exeName)));
            }

            await Task.WhenAll(fileLoaders);

            for (int i = 0; i < mkvFiles.Length; i++)
            {
                txtFile.AppendText(mkvFiles[i]);                // Add current file's path to file name TextBox
                if (i < (mkvFiles.Length - 1))                  // If the file is not the last one...
                    txtFile.AppendText(Environment.NewLine);    // Also add a new line for nextfile name
            }

            maxVideoIndex = Array.IndexOf(videoCounts, videoCounts.Max());
            maxAudioIndex = Array.IndexOf(audioCounts, audioCounts.Max());
            maxSubIndex = Array.IndexOf(subCounts, subCounts.Max());

            List<VideoTrack> videoTracks = new List<VideoTrack>() { new VideoTrack() };
            List<AudioTrack> audioTracks = new List<AudioTrack>() { new AudioTrack() };
            List<SubtitleTrack> subTracks = new List<SubtitleTrack>() { new SubtitleTrack() };

            myDebug.WriteLine($"VIDEO:\t{ maxVideoIndex }:{ videoCounts[maxVideoIndex] }");
            myDebug.WriteLine($"AUDIO:\t{ maxAudioIndex }:{ audioCounts[maxAudioIndex] }");
            myDebug.WriteLine($"SUB  :\t{ maxSubIndex }:{ subCounts[maxSubIndex] }");

            if (maxAudioIndex == maxVideoIndex && maxAudioIndex == maxSubIndex) // best case, all same
            {
                // Get dynamic object from JSON string from mkvmerge.exe for file in folder with max amount of audio tracks
                json = Utils.GetJsonObject(Utils.RunProcess(exeName, "-i -F json \"" + mkvFiles[maxAudioIndex] + "\""));
                videoTracks.AddRange(GetTracks(json, MKVToolNix.TRACK_VIDEO));
                audioTracks.AddRange(GetTracks(json, MKVToolNix.TRACK_AUDIO));
                subTracks.AddRange(GetTracks(json, MKVToolNix.TRACK_SUB));
            }
            else
            {
                if (maxAudioIndex == maxSubIndex) // 2/3 case, video different
                {
                    json = Utils.GetJsonObject(Utils.RunProcess(exeName, "-i -F json \"" + mkvFiles[maxAudioIndex] + "\""));
                    audioTracks.AddRange(GetTracks(json, MKVToolNix.TRACK_AUDIO));
                    subTracks.AddRange(GetTracks(json, MKVToolNix.TRACK_SUB));

                    json = Utils.GetJsonObject(Utils.RunProcess(exeName, "-i -F json \"" + mkvFiles[maxVideoIndex] + "\""));
                    videoTracks.AddRange(GetTracks(json, MKVToolNix.TRACK_VIDEO));
                }
                else if (maxVideoIndex == maxSubIndex) // 2/3 case, audio different
                {
                    json = Utils.GetJsonObject(Utils.RunProcess(exeName, "-i -F json \"" + mkvFiles[maxVideoIndex] + "\""));
                    videoTracks.AddRange(GetTracks(json, MKVToolNix.TRACK_VIDEO));
                    subTracks.AddRange(GetTracks(json, MKVToolNix.TRACK_SUB));

                    json = Utils.GetJsonObject(Utils.RunProcess(exeName, "-i -F json \"" + mkvFiles[maxAudioIndex] + "\""));
                    audioTracks.AddRange(GetTracks(json, MKVToolNix.TRACK_AUDIO));
                }
                else if (maxAudioIndex == maxVideoIndex) // 2/3 case, sub different
                {
                    json = Utils.GetJsonObject(Utils.RunProcess(exeName, "-i -F json \"" + mkvFiles[maxAudioIndex] + "\""));
                    audioTracks.AddRange(GetTracks(json, MKVToolNix.TRACK_AUDIO));
                    videoTracks.AddRange(GetTracks(json, MKVToolNix.TRACK_VIDEO));

                    json = Utils.GetJsonObject(Utils.RunProcess(exeName, "-i -F json \"" + mkvFiles[maxSubIndex] + "\""));
                    subTracks.AddRange(GetTracks(json, MKVToolNix.TRACK_SUB));
                }
                else // worst case, all different
                {
                    videoTracks.AddRange(GetTracks(Utils.GetJsonObject(Utils.RunProcess(exeName, "-i -F json \"" + mkvFiles[maxVideoIndex] + "\"")), MKVToolNix.TRACK_VIDEO));
                    audioTracks.AddRange(GetTracks(Utils.GetJsonObject(Utils.RunProcess(exeName, "-i -F json \"" + mkvFiles[maxAudioIndex] + "\"")), MKVToolNix.TRACK_AUDIO));
                    subTracks.AddRange(GetTracks(Utils.GetJsonObject(Utils.RunProcess(exeName, "-i -F json \"" + mkvFiles[maxSubIndex] + "\"")), MKVToolNix.TRACK_SUB));
                }
            }

            RefreshDataGridView(dgvVideo, videoTracks);     // Load Tracks into DataGridViews
            RefreshDataGridView(dgvAudio, audioTracks);
            RefreshDataGridView(dgvSubtitle, subTracks);

            if (mkvFiles.Length > 1)
                lblTitle.Text = "(Multiple files are loaded)";         // Change title label to inform that there are multiple files loadad
            else
            {
                try
                {
                    lblTitle.Text = json["container"]["properties"]["title"];   // Try to display video Title from JSON Object
                }
                catch (KeyNotFoundException)
                {
                    myDebug.WriteLine("This mkv has no Title set, using blank one... ");
                    lblTitle.Text = "";                                         // Display blank title if not present
                }
                txtFile.Text = mkvFiles[0];                                        // Put file name in file name textbox
            }

            UnlockFormControls(true);
        }


        /// <summary>
        /// Method to save the currently selected default track configuration to file(s)
        /// </summary>
        private async Task SaveAllFilesAsync()
        {
            UnlockFormControls(false);

            if (txtFile.Text != "")                             // If the filename label actually has text in it... (meaning something is actually loaded)
            {
                rtxLog.Text = "";                               // Clear the log

                string retStr;                                  // Initialize retStr (used to hold string returned by Utils.RunProcess)
                string[] mkvFiles = txtFile.Text.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);  // Build array of filenames from file name TextBox
                string exeName = "\"" + MKVToolNix.ExeGetPath(MKVToolNix.mkvpropeditExe) + "\"";    // Initialize name for exe for Utils.RunProcess, grabbing path for currently installed mkvpropedit.exe and wrapping in quotes

                StringBuilder args = new StringBuilder();       // Instantiate new StringBuilder to construct final arguments string

                if (chkVideoEn.Checked)
                    args.Append(BuildArgs(dgvVideo, false, MKVToolNix.TRACK_VIDEO));       // Call BuildArgs for all video track args and append to StringBuilder
                if (chkAudioEn.Checked)
                    args.Append(BuildArgs(dgvAudio, false, MKVToolNix.TRACK_AUDIO));       // Call BuildArgs for all audio track args and append to StringBuilder
                if (chkSubEn.Checked)
                    args.Append(BuildArgs(dgvSubtitle, false, MKVToolNix.TRACK_SUB));      // Call BuildArgs for all subtitle track args and append to StringBuilder

                if (mkvFiles.Length > 1)                              // If the batch mode option is on...
                {
                    filesModified = 0;                                              // Initialize counter to count how many files were successfully modified
                    List<Task<string>> filesSaving = new List<Task<string>>();

                    for (int i = 0; i < mkvFiles.Length; i++)                            // For each file...
                    {
                        int idx = i;
                        int count = mkvFiles.Length;
                        string path = mkvFiles[i];
                        string argsStr = args.ToString();
                        myDebug.WriteLine(path);
                        filesSaving.Add(Task.Run(() => SaveFile(path, argsStr, exeName, idx, count)));
                    }
                    var results = await Task.WhenAll(filesSaving);
                    foreach (string s in results)
                        rtxLog.AppendText(s);

                    // Finally show message to user when batch operation is done including count of files successfully modified
                    MessageBox.Show(filesModified + " of " + mkvFiles.Length + " files successfully modified!", "Batch save complete!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {   // Else if the batch mode is off, just do the following
                    string fileName = mkvFiles[0];                                          // If there is only one file, just get filename from array
                    string wrappedFileName = "\"" + fileName + "\"";                        // Wrap filename in quotes for RunProcess arg
                    rtxLog.SendToLog("File: " + wrappedFileName);                           // Send file name to log
                    rtxLog.SendToLog(exeName + " " + wrappedFileName + args.ToString());    // Send complete mkvpropedit arg to log
                    retStr = Utils.RunProcess(exeName, wrappedFileName + args.ToString());  // Run mkvpropedit with arg
                    rtxLog.SendToLog(retStr);                                               // send returned result to log
                }
            }

            UnlockFormControls(true);
        }
        //private void SaveAllFiles()
        //{
        //    UnlockFormControls(false);

        //    if (txtFile.Text != "")                             // If the filename label actually has text in it... (meaning something is actually loaded)
        //    {
        //        rtxLog.Text = "";                               // Clear the log

        //        string retStr;                                  // Initialize retStr (used to hold string returned by Utils.RunProcess)
        //        string[] mkvFiles = txtFile.Text.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);  // Build array of filenames from file name TextBox
        //        string exeName = "\"" + MKVToolNix.ExeGetPath(MKVToolNix.mkvpropeditExe) + "\"";    // Initialize name for exe for Utils.RunProcess, grabbing path for currently installed mkvpropedit.exe and wrapping in quotes

        //        StringBuilder args = new StringBuilder();       // Instantiate new StringBuilder to construct final arguments string

        //        if (chkVideoEn.Checked)
        //            args.Append(BuildArgs(dgvVideo, false, MKVToolNix.TRACK_VIDEO));       // Call BuildArgs for all video track args and append to StringBuilder
        //        if (chkAudioEn.Checked)
        //            args.Append(BuildArgs(dgvAudio, false, MKVToolNix.TRACK_AUDIO));       // Call BuildArgs for all audio track args and append to StringBuilder
        //        if (chkSubEn.Checked)
        //            args.Append(BuildArgs(dgvSubtitle, false, MKVToolNix.TRACK_SUB));      // Call BuildArgs for all subtitle track args and append to StringBuilder

        //        if (mkvFiles.Length > 1)                              // If the "User Is Insane" batch mode option is on...
        //        {
        //            filesModified = 0;                                              // Initialize counter to count how many files were successfully modified

        //            for(int i = 0; i < mkvFiles.Length; i++)                            // For each file...
        //            {
        //                string fileName = "\"" + mkvFiles[i] + "\"";                           // Wrap file name in quotes for final arg
        //                rtxLog.SendToLog("File No." + (i + 1) + " of " + mkvFiles.Length + ": " + mkvFiles[i]); // Send to log file no. and name
        //                rtxLog.SendToLog(exeName + " " + fileName + args.ToString());   // Send to log the complete command sent to mkvpropedit
        //                retStr = Utils.RunProcess(exeName, fileName + args.ToString()); // Run mkvpropedit with final generated args
        //                rtxLog.SendToLog(retStr);                                       // send returned result from mkvpropedit to log

        //                int goodVideoCount = dgvVideo.Rows.Count;                       // Initialize count of "good" video, count of subtitle tracks found in file
        //                int goodAudioCount = dgvAudio.Rows.Count;                       // Initialize count of "good" audio, count of audio tracks found in file (needed for batch mode, may not have all tracks in selected file) 
        //                int goodSubCount = dgvSubtitle.Rows.Count;                      // Initialize count of "good" subtitle, count of subtitle tracks found in file
        //                StringBuilder argsAlt;                                          // Declare another StringBuilder for an alternate final arg (used for batch mode, in case some tracks are not found)

        //                filesModified++;                                                // Increment filesModified

        //                while (retStr != MKVToolNix.mkvpropedit_OK)               // While mkvpropedit does not return the "everything completed OK" result
        //                {
        //                    if (retStr.Contains(MKVToolNix.mkvpropedit_ERROR_CannotOpenFile))
        //                    {
        //                        rtxLog.SendToLog("ERROR: could not seem to access this file, skipping it...");
        //                        filesModified--;
        //                        break;
        //                    }

        //                    if (retStr.Contains(MKVToolNix.mkvPropedit_ERROR_MissingVideoTrack))
        //                        goodVideoCount--;
        //                    else if (retStr.Contains(MKVToolNix.mkvPropedit_ERROR_MissingAudioTrack))          // If returned error was an audio track not found...
        //                        goodAudioCount--;                                       // Means the last audio track was not found, so decrement good audio count
        //                    else if (retStr.Contains(MKVToolNix.mkvPropedit_ERROR_MissingSubTrack))     // If returned error was a subtitle track not found...
        //                        goodSubCount--;                                         // Means the last subtitle track was not found, so decrement good subtitle count

        //                    if (goodVideoCount < (dgvVideo.CurrentRow.Index+1) || goodAudioCount < (dgvAudio.CurrentRow.Index + 1) || goodSubCount < (dgvAudio.CurrentRow.Index + 1))    // If the selected default tracks are not in the "good" tracks available in file...
        //                    {
        //                        rtxLog.SendToLog("Default track selection does not exist in this file, skipping file...");      // inform user that their selected tracks are not present
        //                        filesModified--;                        // decrement filesModified
        //                        break;                                  // break out of while loop, skipping file
        //                    }

        //                    argsAlt = new StringBuilder();              // Instantiate new StringBuilder for alternate args
        //                    if (chkVideoEn.Checked)
        //                        argsAlt.Append(BuildArgs(dgvVideo, false, MKVToolNix.TRACK_VIDEO, goodVideoCount));
        //                    if (chkAudioEn.Checked)
        //                        argsAlt.Append(BuildArgs(dgvAudio, false, MKVToolNix.TRACK_AUDIO, goodAudioCount));         // Build new audio tracks args based on updated good audio count
        //                    if (chkSubEn.Checked)
        //                        argsAlt.Append(BuildArgs(dgvSubtitle, false, MKVToolNix.TRACK_SUB, goodSubCount));        // Build new subtitle track args based on updated good subtitle count

        //                    rtxLog.SendToLog(exeName + " " + fileName + argsAlt.ToString());    // Send new complete command to log
        //                    retStr = Utils.RunProcess(exeName, fileName + argsAlt.ToString());  // Try running mkvpropedit with new args
        //                    rtxLog.SendToLog(retStr);                                           // send result from mkvpropedit to log
        //                }
        //            }
        //            // Finally show message to user when batch operation is done including count of files successfully modified
        //            MessageBox.Show(filesModified + " of " + mkvFiles.Length + " files successfully modified!", "Batch save complete!", MessageBoxButtons.OK, MessageBoxIcon.Information);
        //        }
        //        else
        //        {   // Else if the batch mode is off, just do the following
        //            string fileName = mkvFiles[0];                                          // If there is only one file, just get filename from array
        //            string wrappedFileName = "\"" + fileName + "\"";                        // Wrap filename in quotes for RunProcess arg
        //            rtxLog.SendToLog("File: " + wrappedFileName);                           // Send file name to log
        //            rtxLog.SendToLog(exeName + " " + wrappedFileName + args.ToString());    // Send complete mkvpropedit arg to log
        //            retStr = Utils.RunProcess(exeName, wrappedFileName + args.ToString());  // Run mkvpropedit with arg
        //            rtxLog.SendToLog(retStr);                                               // send returned result to log
        //        }
        //    }

        //    UnlockFormControls(true);
        //}

        private string SaveFile(string path, string args, string exeName, int idx, int count)
        {
            StringBuilder result = new StringBuilder();
            string retStr = "";
            string fileName = "\"" + path + "\"";    // Wrap file name in quotes for use in args

            result.AppendLine($"File No.{idx + 1} of {count}: {path}"); // Send to log file no. and name
            result.AppendLine($"{exeName} {fileName}{args}");   // Send to log the complete command sent to mkvpropedit
            retStr = Utils.RunProcess(exeName, fileName + args.ToString()); // Run mkvpropedit with final generated args
            result.AppendLine(retStr);                       // send returned result from mkvpropedit to log

            int goodVideoCount = dgvVideo.Rows.Count;                       // Initialize count of "good" video, count of subtitle tracks found in file
            int goodAudioCount = dgvAudio.Rows.Count;                       // Initialize count of "good" audio, count of audio tracks found in file (needed for batch mode, may not have all tracks in selected file) 
            int goodSubCount = dgvSubtitle.Rows.Count;                      // Initialize count of "good" subtitle, count of subtitle tracks found in file
            StringBuilder argsAlt;                                          // Declare another StringBuilder for an alternate final arg (used for batch mode, in case some tracks are not found)

            filesModified++;                                                // Increment filesModified

            while (retStr != MKVToolNix.mkvpropedit_OK)               // While mkvpropedit does not return the "everything completed OK" result
            {
                if (retStr.Contains(MKVToolNix.mkvpropedit_ERROR_CannotOpenFile))
                {
                    result.AppendLine("ERROR: could not seem to access this file, skipping it...");
                    result.AppendLine();
                    filesModified--;
                    break;
                }

                if (retStr.Contains(MKVToolNix.mkvPropedit_ERROR_MissingVideoTrack))
                    goodVideoCount--;
                else if (retStr.Contains(MKVToolNix.mkvPropedit_ERROR_MissingAudioTrack))          // If returned error was an audio track not found...
                    goodAudioCount--;                                       // Means the last audio track was not found, so decrement good audio count
                else if (retStr.Contains(MKVToolNix.mkvPropedit_ERROR_MissingSubTrack))     // If returned error was a subtitle track not found...
                    goodSubCount--;                                         // Means the last subtitle track was not found, so decrement good subtitle count

                if (goodVideoCount < (dgvVideo.CurrentRow.Index + 1) || goodAudioCount < (dgvAudio.CurrentRow.Index + 1) || goodSubCount < (dgvAudio.CurrentRow.Index + 1))    // If the selected default tracks are not in the "good" tracks avaible in file...
                {
                    result.AppendLine("Default track selection does not exist in this file, skipping file...");      // inform user that their selected tracks are not present
                    result.AppendLine();
                    filesModified--;                        // decrement filesModified
                    break;                                  // break out of while loop, skipping file
                }

                argsAlt = new StringBuilder();              // Instantiate new StringBuilder for alternate args
                if (chkVideoEn.Checked)
                    argsAlt.Append(BuildArgs(dgvVideo, false, MKVToolNix.TRACK_VIDEO, goodVideoCount));
                if (chkAudioEn.Checked)
                    argsAlt.Append(BuildArgs(dgvAudio, false, MKVToolNix.TRACK_AUDIO, goodAudioCount));         // Build new audio tracks args based on updated good audio count
                if (chkSubEn.Checked)
                    argsAlt.Append(BuildArgs(dgvSubtitle, false, MKVToolNix.TRACK_SUB, goodSubCount));        // Build new subtitle track args based on updated good subtitle count

                result.AppendLine($"{exeName} {fileName}{argsAlt.ToString()}");    // Send new complete command to log
                retStr = Utils.RunProcess(exeName, fileName + argsAlt.ToString());  // Try running mkvpropedit with new args
                result.AppendLine(retStr);                                           // send result from mkvpropedit to log
            }
            return result.ToString();
        }

        

        /// <summary>
        /// Method to build the arguments to tell mkvpropedit to edit which tracks, and with what values
        /// </summary>
        /// <param name="dgv">The DataGridView containing the tracks to generate arguments for.</param>
        /// <param name="addSetting">Boolean to specify whether the argument edits the existing setting in the track in file, or adds it anew. Used for when the setting does not exist in the track in file.</param>
        /// <param name="type">The string representing which Track SubType is used for the arguument.</param>
        /// <param name="trackCount">Optional. Used to specify how many tracks in the ListBox are being processed. Will use the ListBox's Item Count as default.</param>
        /// <returns>String of arguments for editing the tracks in the ListBox</returns>
        private string BuildArgs(DataGridView dgv, bool addSetting, string type, int trackCount = 0)
        {
            if (trackCount == 0)                    // If trackCount is defaul sentinel value of zero
                trackCount = dgv.Rows.Count;   // change it to the listBox's Item Count

            string arg = "";                        // initialize the string vars, the arg, its track type, and its setting type
            string tType = "";
            string sType = "";

            switch (type)
            {
                case MKVToolNix.TRACK_VIDEO:
                    tType = "v";                    // set arg track type to video
                    break;

                case MKVToolNix.TRACK_AUDIO:
                    tType = "a";                    // set arg track type to audio
                    break;

                case MKVToolNix.TRACK_SUB:
                    tType = "s";                    // set arg track type to subtitle
                    break;
            }

            for (int i = 1; i < trackCount; i++)    // For each of the tracks (up to the specified count)
            {
                bool def = false;                   // Initialize boolean to represent part of argument for track's default flag
                                                    // Default is false
                if (i == dgv.SelectedRows[0].Index)     // But if the current track is selected in the listBox...
                    def = true;                     // set the default flag to true;

                if (addSetting)                     // If addSetting is true...
                    sType = "a";                    // Set argument's setting type to "add"
                else
                    sType = "s";                    // Otherwise set argument's setting type to "set"

                // Construct final argument for this track and add it to arg string
                arg += " -e track:" + tType + i + " -" + sType + " flag-default=" + Convert.ToInt16(def);
            }
            return arg;                             // Return the completed arg string string
        }

        /// <summary>
        /// Method to refresh and re-bind a List of Strings to a corresponding DataGridView control on the form, and auto-select the default track.
        /// </summary>
        /// <param name="dgv">The DataGridView to bind the Tracks to.</param>
        /// <param name="tracks">The Collection of Tracks to load into the DataGridView (need to all be same SubType)</param>
        private void RefreshDataGridView(DataGridView dgv, IEnumerable<Track> tracks)
        {
            dgv.AutoGenerateColumns = false;
            dgv.DataSource = null;
            dgv.Columns.Clear();
            dgv.Columns.Add(new DataGridViewTextBoxColumn() { DataPropertyName = "Language", HeaderText = "Language", AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells });
            dgv.Columns.Add(new DataGridViewTextBoxColumn() { DataPropertyName = "Format", HeaderText = "Format", AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells });
            if (tracks.First().GetType() == typeof(AudioTrack))
                dgv.Columns.Add(new DataGridViewTextBoxColumn() { DataPropertyName = "Channels", HeaderText = "Channels", AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells });
            dgv.Columns.Add(new DataGridViewTextBoxColumn() { DataPropertyName = "Name", HeaderText = "Name", AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells });
            dgv.DataSource = tracks;

            int i = 0;
            foreach (var t in tracks)
            {
                if (t.IsDefault)
                {
                    dgv.CurrentCell = dgv.Rows[i].Cells[0];
                    return;
                }
                else
                    i++;
            }
            dgv.CurrentCell = dgv.Rows[0].Cells[0];
        }

        /// <summary>
        /// Method to lock or unlock certain form controls, useful to prevent user from trying stuffif certain conditions are not met
        /// </summary>
        /// <param name="unlocked">Boolean value representing wether or notto uunlock certain controls</param>
        private void UnlockFormControls(bool unlocked)
        {
            loadToolStripMenuItem.Enabled = unlocked;   // Enable or disable the following controls depending on the boool passed toit
            saveToolStripMenuItem.Enabled = unlocked;
            btnLoad.Enabled = unlocked;
            btnSave.Enabled = unlocked;
            gbxVideo.Enabled = unlocked;
            gbxAudio.Enabled = unlocked;
            gbxSubtitle.Enabled = unlocked;
        }

        #endregion methods

        private void chkVideoEn_CheckedChanged(object sender, EventArgs e)
        {
            dgvVideo.Enabled = chkVideoEn.Checked;
            dgvVideo.EnableHeadersVisualStyles = chkVideoEn.Checked;
        }

        private void chkAudioEn_CheckedChanged(object sender, EventArgs e)
        {
            dgvAudio.Enabled = chkAudioEn.Checked;
            dgvAudio.EnableHeadersVisualStyles = chkAudioEn.Checked;
        }

        private void chkSubEn_CheckedChanged(object sender, EventArgs e)
        {
            dgvSubtitle.Enabled = chkSubEn.Checked;
            dgvSubtitle.EnableHeadersVisualStyles = chkSubEn.Checked;
        }
    }
}
