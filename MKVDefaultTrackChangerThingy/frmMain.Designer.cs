﻿namespace MKVDefaultTrackChangerThingy
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnLoad = new System.Windows.Forms.Button();
            this.ofdMKV = new System.Windows.Forms.OpenFileDialog();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblTitle = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitProgramToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mKVToolNixToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.goToMKVToolNixsHomepageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.goToMKVToolNixdownloadPageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recheckToSeeIfMKVToolNixIsInstalledOnThisMachineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rtxLog = new System.Windows.Forms.RichTextBox();
            this.lblAppInfo = new System.Windows.Forms.Label();
            this.llbMKVToolNix = new System.Windows.Forms.LinkLabel();
            this.txtFile = new System.Windows.Forms.TextBox();
            this.fbdMKVDir = new System.Windows.Forms.FolderBrowserDialog();
            this.dgvAudio = new System.Windows.Forms.DataGridView();
            this.dgvVideo = new System.Windows.Forms.DataGridView();
            this.dgvSubtitle = new System.Windows.Forms.DataGridView();
            this.gbxVideo = new System.Windows.Forms.GroupBox();
            this.chkVideoEn = new System.Windows.Forms.CheckBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.gbxAudio = new System.Windows.Forms.GroupBox();
            this.chkAudioEn = new System.Windows.Forms.CheckBox();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.gbxSubtitle = new System.Windows.Forms.GroupBox();
            this.chkSubEn = new System.Windows.Forms.CheckBox();
            this.gbxLog = new System.Windows.Forms.GroupBox();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAudio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVideo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSubtitle)).BeginInit();
            this.gbxVideo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.gbxAudio.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.gbxSubtitle.SuspendLayout();
            this.gbxLog.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).BeginInit();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.Panel2.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(13, 74);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(75, 23);
            this.btnLoad.TabIndex = 1;
            this.btnLoad.Text = "L&oad";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // ofdMKV
            // 
            this.ofdMKV.Filter = "Matroska Video files|*.mkv";
            this.ofdMKV.Multiselect = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "File:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 58);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Title:";
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Location = new System.Drawing.Point(42, 58);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(0, 13);
            this.lblTitle.TabIndex = 8;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(94, 74);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 10;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.mKVToolNixToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(496, 24);
            this.menuStrip1.TabIndex = 11;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.exitProgramToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.loadToolStripMenuItem.Text = "L&oad data from MKV...";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.saveToolStripMenuItem.Text = "&Save Settings to MKV";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // exitProgramToolStripMenuItem
            // 
            this.exitProgramToolStripMenuItem.Name = "exitProgramToolStripMenuItem";
            this.exitProgramToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.exitProgramToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.exitProgramToolStripMenuItem.Text = "E&xit Program";
            this.exitProgramToolStripMenuItem.Click += new System.EventHandler(this.exitProgramToolStripMenuItem_Click);
            // 
            // mKVToolNixToolStripMenuItem
            // 
            this.mKVToolNixToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.goToMKVToolNixsHomepageToolStripMenuItem,
            this.goToMKVToolNixdownloadPageToolStripMenuItem,
            this.recheckToSeeIfMKVToolNixIsInstalledOnThisMachineToolStripMenuItem});
            this.mKVToolNixToolStripMenuItem.Name = "mKVToolNixToolStripMenuItem";
            this.mKVToolNixToolStripMenuItem.Size = new System.Drawing.Size(84, 20);
            this.mKVToolNixToolStripMenuItem.Text = "&MKVToolNix";
            // 
            // goToMKVToolNixsHomepageToolStripMenuItem
            // 
            this.goToMKVToolNixsHomepageToolStripMenuItem.Name = "goToMKVToolNixsHomepageToolStripMenuItem";
            this.goToMKVToolNixsHomepageToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.H)));
            this.goToMKVToolNixsHomepageToolStripMenuItem.Size = new System.Drawing.Size(329, 22);
            this.goToMKVToolNixsHomepageToolStripMenuItem.Text = "Go to MKVToolNix &Homepage...";
            this.goToMKVToolNixsHomepageToolStripMenuItem.Click += new System.EventHandler(this.goToMKVToolNixsHomepageToolStripMenuItem_Click);
            // 
            // goToMKVToolNixdownloadPageToolStripMenuItem
            // 
            this.goToMKVToolNixdownloadPageToolStripMenuItem.Name = "goToMKVToolNixdownloadPageToolStripMenuItem";
            this.goToMKVToolNixdownloadPageToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.goToMKVToolNixdownloadPageToolStripMenuItem.Size = new System.Drawing.Size(329, 22);
            this.goToMKVToolNixdownloadPageToolStripMenuItem.Text = "Go to MKVToolNix &Download page...";
            this.goToMKVToolNixdownloadPageToolStripMenuItem.Click += new System.EventHandler(this.goToMKVToolNixdownloadPageToolStripMenuItem_Click);
            // 
            // recheckToSeeIfMKVToolNixIsInstalledOnThisMachineToolStripMenuItem
            // 
            this.recheckToSeeIfMKVToolNixIsInstalledOnThisMachineToolStripMenuItem.Name = "recheckToSeeIfMKVToolNixIsInstalledOnThisMachineToolStripMenuItem";
            this.recheckToSeeIfMKVToolNixIsInstalledOnThisMachineToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R)));
            this.recheckToSeeIfMKVToolNixIsInstalledOnThisMachineToolStripMenuItem.Size = new System.Drawing.Size(329, 22);
            this.recheckToSeeIfMKVToolNixIsInstalledOnThisMachineToolStripMenuItem.Text = "&Recheck to see if MKVToolNix is installed";
            this.recheckToSeeIfMKVToolNixIsInstalledOnThisMachineToolStripMenuItem.Click += new System.EventHandler(this.recheckToSeeIfMKVToolNixIsInstalledOnThisMachineToolStripMenuItem_Click);
            // 
            // rtxLog
            // 
            this.rtxLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxLog.Location = new System.Drawing.Point(3, 16);
            this.rtxLog.Name = "rtxLog";
            this.rtxLog.Size = new System.Drawing.Size(490, 147);
            this.rtxLog.TabIndex = 15;
            this.rtxLog.Text = "";
            // 
            // lblAppInfo
            // 
            this.lblAppInfo.AutoSize = true;
            this.lblAppInfo.Location = new System.Drawing.Point(13, 11);
            this.lblAppInfo.Name = "lblAppInfo";
            this.lblAppInfo.Size = new System.Drawing.Size(91, 13);
            this.lblAppInfo.TabIndex = 16;
            this.lblAppInfo.Text = " by damysteryman";
            // 
            // llbMKVToolNix
            // 
            this.llbMKVToolNix.AutoSize = true;
            this.llbMKVToolNix.Dock = System.Windows.Forms.DockStyle.Right;
            this.llbMKVToolNix.LinkArea = new System.Windows.Forms.LinkArea(13, 10);
            this.llbMKVToolNix.Location = new System.Drawing.Point(366, 0);
            this.llbMKVToolNix.Name = "llbMKVToolNix";
            this.llbMKVToolNix.Size = new System.Drawing.Size(130, 30);
            this.llbMKVToolNix.TabIndex = 17;
            this.llbMKVToolNix.TabStop = true;
            this.llbMKVToolNix.Text = "\r\nPowered by MKVToolNix";
            this.llbMKVToolNix.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.llbMKVToolNix.UseCompatibleTextRendering = true;
            this.llbMKVToolNix.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.llbMKVToolNix_LinkClicked);
            // 
            // txtFile
            // 
            this.txtFile.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtFile.HideSelection = false;
            this.txtFile.Location = new System.Drawing.Point(45, 34);
            this.txtFile.Multiline = true;
            this.txtFile.Name = "txtFile";
            this.txtFile.ReadOnly = true;
            this.txtFile.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtFile.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtFile.Size = new System.Drawing.Size(451, 20);
            this.txtFile.TabIndex = 18;
            this.txtFile.WordWrap = false;
            // 
            // dgvAudio
            // 
            this.dgvAudio.AllowUserToAddRows = false;
            this.dgvAudio.AllowUserToDeleteRows = false;
            this.dgvAudio.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvAudio.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAudio.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAudio.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvAudio.Location = new System.Drawing.Point(3, 16);
            this.dgvAudio.Name = "dgvAudio";
            this.dgvAudio.ReadOnly = true;
            this.dgvAudio.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAudio.Size = new System.Drawing.Size(490, 137);
            this.dgvAudio.TabIndex = 19;
            this.dgvAudio.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAudio_CellContentClick);
            // 
            // dgvVideo
            // 
            this.dgvVideo.AllowUserToAddRows = false;
            this.dgvVideo.AllowUserToDeleteRows = false;
            this.dgvVideo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvVideo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVideo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvVideo.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvVideo.Location = new System.Drawing.Point(3, 16);
            this.dgvVideo.Name = "dgvVideo";
            this.dgvVideo.ReadOnly = true;
            this.dgvVideo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvVideo.Size = new System.Drawing.Size(490, 138);
            this.dgvVideo.TabIndex = 20;
            // 
            // dgvSubtitle
            // 
            this.dgvSubtitle.AllowUserToAddRows = false;
            this.dgvSubtitle.AllowUserToDeleteRows = false;
            this.dgvSubtitle.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvSubtitle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSubtitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvSubtitle.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSubtitle.Location = new System.Drawing.Point(3, 16);
            this.dgvSubtitle.Name = "dgvSubtitle";
            this.dgvSubtitle.ReadOnly = true;
            this.dgvSubtitle.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSubtitle.Size = new System.Drawing.Size(490, 140);
            this.dgvSubtitle.TabIndex = 21;
            // 
            // gbxVideo
            // 
            this.gbxVideo.Controls.Add(this.chkVideoEn);
            this.gbxVideo.Controls.Add(this.dgvVideo);
            this.gbxVideo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbxVideo.Location = new System.Drawing.Point(0, 0);
            this.gbxVideo.Name = "gbxVideo";
            this.gbxVideo.Size = new System.Drawing.Size(496, 157);
            this.gbxVideo.TabIndex = 22;
            this.gbxVideo.TabStop = false;
            this.gbxVideo.Text = "Video Tracks";
            // 
            // chkVideoEn
            // 
            this.chkVideoEn.AutoSize = true;
            this.chkVideoEn.Checked = true;
            this.chkVideoEn.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkVideoEn.Location = new System.Drawing.Point(89, -1);
            this.chkVideoEn.Name = "chkVideoEn";
            this.chkVideoEn.Size = new System.Drawing.Size(65, 17);
            this.chkVideoEn.TabIndex = 21;
            this.chkVideoEn.Text = "Enabled";
            this.chkVideoEn.UseVisualStyleBackColor = true;
            this.chkVideoEn.CheckedChanged += new System.EventHandler(this.chkVideoEn_CheckedChanged);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer3);
            this.splitContainer1.Size = new System.Drawing.Size(496, 650);
            this.splitContainer1.SplitterDistance = 317;
            this.splitContainer1.TabIndex = 23;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.gbxVideo);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.gbxAudio);
            this.splitContainer2.Size = new System.Drawing.Size(496, 317);
            this.splitContainer2.SplitterDistance = 157;
            this.splitContainer2.TabIndex = 24;
            // 
            // gbxAudio
            // 
            this.gbxAudio.Controls.Add(this.chkAudioEn);
            this.gbxAudio.Controls.Add(this.dgvAudio);
            this.gbxAudio.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbxAudio.Location = new System.Drawing.Point(0, 0);
            this.gbxAudio.Name = "gbxAudio";
            this.gbxAudio.Size = new System.Drawing.Size(496, 156);
            this.gbxAudio.TabIndex = 26;
            this.gbxAudio.TabStop = false;
            this.gbxAudio.Text = "Audio Tracks";
            // 
            // chkAudioEn
            // 
            this.chkAudioEn.AutoSize = true;
            this.chkAudioEn.Checked = true;
            this.chkAudioEn.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAudioEn.Location = new System.Drawing.Point(89, -1);
            this.chkAudioEn.Name = "chkAudioEn";
            this.chkAudioEn.Size = new System.Drawing.Size(65, 17);
            this.chkAudioEn.TabIndex = 22;
            this.chkAudioEn.Text = "Enabled";
            this.chkAudioEn.UseVisualStyleBackColor = true;
            this.chkAudioEn.CheckedChanged += new System.EventHandler(this.chkAudioEn_CheckedChanged);
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.gbxSubtitle);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.gbxLog);
            this.splitContainer3.Size = new System.Drawing.Size(496, 329);
            this.splitContainer3.SplitterDistance = 159;
            this.splitContainer3.TabIndex = 25;
            // 
            // gbxSubtitle
            // 
            this.gbxSubtitle.Controls.Add(this.chkSubEn);
            this.gbxSubtitle.Controls.Add(this.dgvSubtitle);
            this.gbxSubtitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbxSubtitle.Location = new System.Drawing.Point(0, 0);
            this.gbxSubtitle.Name = "gbxSubtitle";
            this.gbxSubtitle.Size = new System.Drawing.Size(496, 159);
            this.gbxSubtitle.TabIndex = 26;
            this.gbxSubtitle.TabStop = false;
            this.gbxSubtitle.Text = "Subtitle Tracks";
            // 
            // chkSubEn
            // 
            this.chkSubEn.AutoSize = true;
            this.chkSubEn.Checked = true;
            this.chkSubEn.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSubEn.Location = new System.Drawing.Point(89, -1);
            this.chkSubEn.Name = "chkSubEn";
            this.chkSubEn.Size = new System.Drawing.Size(65, 17);
            this.chkSubEn.TabIndex = 22;
            this.chkSubEn.Text = "Enabled";
            this.chkSubEn.UseVisualStyleBackColor = true;
            this.chkSubEn.CheckedChanged += new System.EventHandler(this.chkSubEn_CheckedChanged);
            // 
            // gbxLog
            // 
            this.gbxLog.Controls.Add(this.rtxLog);
            this.gbxLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbxLog.Location = new System.Drawing.Point(0, 0);
            this.gbxLog.Name = "gbxLog";
            this.gbxLog.Size = new System.Drawing.Size(496, 166);
            this.gbxLog.TabIndex = 0;
            this.gbxLog.TabStop = false;
            this.gbxLog.Text = "mkvpropedit.exe File Edit Output Log:";
            // 
            // splitContainer4
            // 
            this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer4.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer4.IsSplitterFixed = true;
            this.splitContainer4.Location = new System.Drawing.Point(0, 24);
            this.splitContainer4.Name = "splitContainer4";
            this.splitContainer4.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.Controls.Add(this.lblAppInfo);
            this.splitContainer4.Panel1.Controls.Add(this.btnSave);
            this.splitContainer4.Panel1.Controls.Add(this.txtFile);
            this.splitContainer4.Panel1.Controls.Add(this.lblTitle);
            this.splitContainer4.Panel1.Controls.Add(this.btnLoad);
            this.splitContainer4.Panel1.Controls.Add(this.label4);
            this.splitContainer4.Panel1.Controls.Add(this.llbMKVToolNix);
            this.splitContainer4.Panel1.Controls.Add(this.label3);
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.Controls.Add(this.splitContainer1);
            this.splitContainer4.Size = new System.Drawing.Size(496, 754);
            this.splitContainer4.SplitterDistance = 100;
            this.splitContainer4.TabIndex = 24;
            // 
            // frmMain
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(496, 778);
            this.Controls.Add(this.splitContainer4);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.Text = "MKVDefaultTrackChangerThingy";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.frmMain_DragDrop);
            this.DragEnter += new System.Windows.Forms.DragEventHandler(this.frmMain_DragEnter);
            this.Resize += new System.EventHandler(this.frmMain_Resize);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAudio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVideo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSubtitle)).EndInit();
            this.gbxVideo.ResumeLayout(false);
            this.gbxVideo.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.gbxAudio.ResumeLayout(false);
            this.gbxAudio.PerformLayout();
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.gbxSubtitle.ResumeLayout(false);
            this.gbxSubtitle.PerformLayout();
            this.gbxLog.ResumeLayout(false);
            this.splitContainer4.Panel1.ResumeLayout(false);
            this.splitContainer4.Panel1.PerformLayout();
            this.splitContainer4.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).EndInit();
            this.splitContainer4.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.OpenFileDialog ofdMKV;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitProgramToolStripMenuItem;
        private System.Windows.Forms.RichTextBox rtxLog;
        private System.Windows.Forms.ToolStripMenuItem mKVToolNixToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem goToMKVToolNixsHomepageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem goToMKVToolNixdownloadPageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem recheckToSeeIfMKVToolNixIsInstalledOnThisMachineToolStripMenuItem;
        private System.Windows.Forms.Label lblAppInfo;
        private System.Windows.Forms.LinkLabel llbMKVToolNix;
        private System.Windows.Forms.TextBox txtFile;
        private System.Windows.Forms.FolderBrowserDialog fbdMKVDir;
        private System.Windows.Forms.DataGridView dgvAudio;
        private System.Windows.Forms.DataGridView dgvVideo;
        private System.Windows.Forms.DataGridView dgvSubtitle;
        private System.Windows.Forms.GroupBox gbxVideo;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.GroupBox gbxAudio;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.GroupBox gbxSubtitle;
        private System.Windows.Forms.GroupBox gbxLog;
        private System.Windows.Forms.SplitContainer splitContainer4;
        private System.Windows.Forms.CheckBox chkVideoEn;
        private System.Windows.Forms.CheckBox chkAudioEn;
        private System.Windows.Forms.CheckBox chkSubEn;
    }
}

