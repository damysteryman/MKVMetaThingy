﻿using MKVMetaThingyUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKVDefaultTrackChangerThingy
{
    public abstract class Track
    {
        public string Name { get; set; }
        public string Language { get; set; }
        public string Format { get; set; }
        public bool IsDefault { get; set; }

        public Track()
        {
            Name = "(none)";
            Language = "(none)";
            Format = "(none)";
            IsDefault = false;
        }

        public Track(string name, string lang, string format, bool isDefault)
        {
            Name = name;
            Language = lang;
            Format = format;
            IsDefault = isDefault;
        }
    }

    public class VideoTrack : Track
    {
        public VideoTrack() : base() { }
        public VideoTrack(string name, string lang, string format, bool isDefault) : base(name, lang, format, isDefault) { }
    }

    public class AudioTrack : Track
    {
        public int Channels { get; set; }

        public AudioTrack() : base()
        {
            Channels = 0;
        }

        public AudioTrack(string name, string lang, string format, bool isDefault, int channels) : base(name, lang, format, isDefault)
        {
            Channels = channels;
        }
    }

    public class SubtitleTrack : Track
    {
        public SubtitleTrack() : base() { }
        public SubtitleTrack(string name, string lang, string format, bool isDefault) : base(name, lang, format, isDefault) { }
    }

    public class InvalidTrackTypeException : Exception
    {
        public InvalidTrackTypeException(string message) : base(message) { }
    }

    public static class TrackFactory
    {
        public static Track MakeTrack(dynamic json)
        {
            string type = json["type"];

            string name;
            bool isDefault;

            try
            {
                name = json["properties"]["track_name"];      // Try to get the track name from dynamic object
            }
            catch (KeyNotFoundException)
            {
                myDebug.WriteLine("Track has no name... using blank name instead.");
                name = "";                                                 // If it is not even present in object, use blank name instead
            }

            try
            {
                isDefault = json["properties"]["default_track"];  // Try to get the default track flag value from dynamic object
            }
            catch (KeyNotFoundException)
            {
                myDebug.WriteLine("Track's \"Default Track\" flag does not even exist... setting default track flag to false.");
                isDefault = false;                                         // If it is not even present in object, use default of false
            }

            string lang = json["properties"]["language"].ToUpper();   // Get track language code from dynamic object
            string format = json["codec"];

            switch (type)
            {
                case MKVToolNix.TRACK_VIDEO:
                    return new VideoTrack(name, lang, format, isDefault);

                case MKVToolNix.TRACK_AUDIO:
                    int channels = json["properties"]["audio_channels"];
                    return new AudioTrack(name, lang, format, isDefault, channels);

                case MKVToolNix.TRACK_SUB:
                    return new SubtitleTrack(name, lang, format, isDefault);

                default:
                    string message = "Invalid/Unsupported track type detected!" + Environment.NewLine + $"Track says it is of type \"{ type }\".";
                    throw new InvalidTrackTypeException(message);
            }
            
                
        }
    }
}
